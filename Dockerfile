FROM openjdk:17-alpine
WORKDIR /app
COPY build/libs/gb_books-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app/app.jar"]