package github.masterj3y.gb_books.controller

import github.masterj3y.gb_books.dto.BookDTO
import github.masterj3y.gb_books.service.BooksService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class BooksController(private val service: BooksService) {

    @PostMapping
    fun addNewBook(@RequestBody book: BookDTO): BookDTO = service.addNewBook(book)

    @GetMapping
    fun findAllBooks(): List<BookDTO> = service.findAllBooks()
}