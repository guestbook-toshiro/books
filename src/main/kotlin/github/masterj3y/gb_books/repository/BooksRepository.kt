package github.masterj3y.gb_books.repository

import github.masterj3y.gb_books.entity.Book
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface BooksRepository: JpaRepository<Book, UUID>