package github.masterj3y.gb_books.service

import github.masterj3y.gb_books.dto.BookDTO
import github.masterj3y.gb_books.entity.Book
import github.masterj3y.gb_books.repository.BooksRepository
import org.springframework.stereotype.Service

@Service
class BooksService(private val repository: BooksRepository) {

    fun addNewBook(bookDTO: BookDTO): BookDTO {
        return repository.save(
            Book(
                title = bookDTO.title,
                pages = bookDTO.pages
            )
        ).let { book -> BookDTO(title = book.title, pages = book.pages) }
    }

    fun findAllBooks(): List<BookDTO> {
        return repository.findAll().map { book ->
            BookDTO(
                title = book.title,
                pages = book.pages
            )
        }
    }
}