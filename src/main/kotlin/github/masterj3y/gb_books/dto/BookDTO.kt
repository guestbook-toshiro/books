package github.masterj3y.gb_books.dto

data class BookDTO(
    val title: String,
    val pages: Int,
)